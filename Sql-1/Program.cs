﻿using Sql_1.src;
using System;

namespace Sql_1
{
    class Program
    {
        static void Main(string[] args)
        {
            UserCommandHandler commandHanlder = new UserCommandHandler();
            commandHanlder.HandleInput();
        }
    }
}
