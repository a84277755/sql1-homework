﻿INSERT INTO category (name) VALUES
('Products'),
('Animals'),
('Cars'),
('Gifts'),
('Books');

INSERT INTO users (name, phone) VALUES
('Artem Gorokhovskii', '+7(999)999-99-99'),
('Ivan Ivanov', '+79999999999'),
('Petr Petrov', '+7(999)999-99-99'),
('Dmitrii Dmitriev', '+7(999)999-99-99'),
('Joe Black', '+7(999)999-99-99');

INSERT INTO ads (name, description, userId, categoryId, price) VALUES
('Toy', 'My favourite toy', 1, 4, 99),
('Bananas', 'Tasty, 1kg', 2, 1, 5),
('Cat', 'Good friend for you', 3, 2, 125),
('BMW', 'New model', 4, 3, 19999),
('1988', 'Collection of jokes', 5, 5, 19999);
