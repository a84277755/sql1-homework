﻿CREATE TABLE category (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255)
);
CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255),
	phone VARCHAR(30)
);
CREATE TABLE ads (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255),
  description TEXT,
  userId int,
  categoryId int,
  price decimal NOT NULL,
  CONSTRAINT users
	FOREIGN KEY(userId)
		REFERENCES users(id),
  CONSTRAINT category
	FOREIGN KEY(categoryId)
		REFERENCES category(id)
);