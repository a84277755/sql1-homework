﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sql_1.src
{
    class UserCommandHandler
    {
        public void HandleInput()
        {
            CommandController cc = new CommandController();

            Console.WriteLine("Enter command. Use \"help\" to see all commands");

            while (true)
            {
                string userString = Console.ReadLine();
                string result = cc.HandleCommand(userString);

                if (result == "quit")
                {
                    break;
                }

                Console.WriteLine(result);
            }
        }
    }
}
