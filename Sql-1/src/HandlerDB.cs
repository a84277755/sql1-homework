﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sql_1.src
{
    class HandlerDB
    {
        private static string cs = null;
        private void GetConnectionString()
        {
            if (cs == null)
            {
                string path = Path.Combine(".", "src", "secrets", "ConnectionStringDB.txt");
                string[] strings = File.ReadAllLines(path);
                cs = strings[0];
            }
        }

        public string GetUsers()
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            string sql = @"SELECT * from users";

            using var executor = new NpgsqlCommand(sql, con).ExecuteReader();

            StringBuilder sb = new StringBuilder("");

            while (executor.Read())
            {
                sb.Append($"{executor.GetInt32(0)} - {executor.GetString(1)} - {executor.GetString(2)}{Environment.NewLine}");
            }

            return sb.ToString().Trim();
        }

        public string GetCategories()
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            string sql = @"SELECT * from category";

            using var executor = new NpgsqlCommand(sql, con).ExecuteReader();

            StringBuilder sb = new StringBuilder("");

            while (executor.Read())
            {
                sb.Append($"{executor.GetInt32(0)} - {executor.GetString(1)}{Environment.NewLine}");
            }

            return sb.ToString().Trim();
        }
        public string GetAds()
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            string sql = @"SELECT * from ads";

            using var executor = new NpgsqlCommand(sql, con).ExecuteReader();

            StringBuilder sb = new StringBuilder("");

            while (executor.Read())
            {
                sb.Append($"{executor.GetInt32(0)} - {executor.GetString(1)} - {executor.GetString(2)} - {executor.GetInt32(3)} - {executor.GetInt32(4)} - {executor.GetFloat(5)}{Environment.NewLine}");
            }

            return sb.ToString().Trim();
        }

        public string InitTables()
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            string sql = File.ReadAllText(Path.Combine(".", "src", "sql", "CreateTables.sql"));

            try
            {
                new NpgsqlCommand(sql, con).ExecuteNonQuery();
            }
            catch (Exception error)
            {
                return "Problem occured!" + Environment.NewLine + error.Message;
            }

            return "OK";
        }

        public string InitDraftData()
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            string sql = File.ReadAllText(Path.Combine(".", "src", "sql", "CreateDraftData.sql"));

            try
            {
                new NpgsqlCommand(sql, con).ExecuteNonQuery();
            }
            catch (Exception error)
            {
                return "Problem occured!" + Environment.NewLine + error.Message;
            }

            return "OK";
        }

        public string AddUser(string name, string phone)
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            using var cmd = new NpgsqlCommand("INSERT INTO users (name, phone) VALUES (@p1, @p2)", con);

            cmd.Parameters.AddWithValue("p1", name);
            cmd.Parameters.AddWithValue("p2", phone);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                return "Problem occured!" + Environment.NewLine + error.Message;
            }

            return "User added";
        }

        public string AddCategory(string name)
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            using var cmd = new NpgsqlCommand("INSERT INTO category (name) VALUES (@p1)", con);
            cmd.Parameters.AddWithValue("p1", name);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                return "Problem occured!" + Environment.NewLine + error.Message;
            }

            return "Category added";
        }

        public string AddAd(string name, string description, int categoryId, int userId, float price)
        {
            GetConnectionString();
            using var con = new NpgsqlConnection(cs);
            con.Open();

            using var cmd = new NpgsqlCommand("INSERT INTO ads (name, description, userId, categoryId, price) VALUES (@p1, @p2, @p3, @p4, @p5)", con);

            cmd.Parameters.AddWithValue("p1", name);
            cmd.Parameters.AddWithValue("p2", description);
            cmd.Parameters.AddWithValue("p3", categoryId);
            cmd.Parameters.AddWithValue("p4", userId);
            cmd.Parameters.AddWithValue("p5", price);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                return "Problem occured!" + Environment.NewLine + error.Message;
            }

            return "Ad added";
        }
    }
}
