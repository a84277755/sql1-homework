﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sql_1.src
{
    class HandlerAddDataLogic
    {
        public string HandleAddingUser ()
        {
            while(true)
            {
                Console.WriteLine("Please, enter data in format: \"name$$phone\"");
                Console.WriteLine("Write \"q\" to escape adding user");

                string userString = Console.ReadLine().Trim();
                if (IsQuitRequested(userString))
                {
                    return null;
                }

                string[] partsOfUserString = userString.Split("$$");
                if (partsOfUserString.Length == 2)
                {
                    return userString;
                }

                Console.WriteLine("Incorret data was entered. Please, use $$ to separate data");
            }
        }

        public string HandleAddingCategory()
        {
            while (true)
            {
                Console.WriteLine("Please, enter data in format: \"name\"");
                Console.WriteLine("Write \"q\" to escape adding user");

                string userString = Console.ReadLine().Trim();
                if (IsQuitRequested(userString))
                {
                    return null;
                }

                string[] partsOfUserString = userString.Split("$$");
                if (partsOfUserString.Length == 1)
                {
                    return userString;
                }

                Console.WriteLine("Incorret data was entered. Please, use $$ to separate data");
            }
        }

        public string HandleAddingAd()
        {
            while (true)
            {
                Console.WriteLine("Please, enter data in format: \"name$$description$$categoryId$$userId$$price\"");
                Console.WriteLine("Write \"q\" to escape adding user");

                string userString = Console.ReadLine().Trim();
                if (IsQuitRequested(userString))
                {
                    return null;
                }

                string[] partsOfUserString = userString.Split("$$");
                if (partsOfUserString.Length == 5)
                {
                    return userString;
                }

                Console.WriteLine("Incorret data was entered. Please, use $$ to separate data");
            }
        }

        private bool IsQuitRequested(string userString)
        {
            return userString == "q";
        }
    }
}
