﻿using System;
using System.Globalization;
using System.Text;

namespace Sql_1.src
{
    class CommandController
    {
        public string HandleCommand(string command)
        {
            HandlerDB handlerDB = new HandlerDB();
            HandlerAddDataLogic handlerAddDataLogic = new HandlerAddDataLogic();

            string result = "";

            switch (command)
            {
                case "q":
                    return "quit";
                case "help":
                    return GetHelp();
                case "users":
                    return handlerDB.GetUsers();
                case "categories":
                    return handlerDB.GetCategories();
                case "ads":
                    return handlerDB.GetAds();
                case "all":
                    StringBuilder sb = new StringBuilder("");
                    sb.Append("Users:" + Environment.NewLine);
                    sb.Append(handlerDB.GetUsers());
                    sb.Append(Environment.NewLine + "Categories:" + Environment.NewLine);
                    sb.Append(handlerDB.GetCategories());
                    sb.Append(Environment.NewLine + "Ads:" + Environment.NewLine);
                    sb.Append(handlerDB.GetAds());
                    return sb.ToString();
                case "init tables":
                    return handlerDB.InitTables();
                case "init draft data":
                    return handlerDB.InitDraftData();
                case "add user":
                    result = handlerAddDataLogic.HandleAddingUser();
                    if (result != null)
                    {
                        string[] args = result.Split("$$");
                        return handlerDB.AddUser(args[0], args[1]);
                    }
                    return "User wasn't added";
                case "add category":
                    result = handlerAddDataLogic.HandleAddingCategory();
                    if (result != null)
                    {
                        string[] args = result.Split("$$");
                        return handlerDB.AddCategory(args[0]);
                    }
                    return "Category wasn't added";
                case "add ad":
                    result = handlerAddDataLogic.HandleAddingAd();
                    if (result != null)
                    {
                        string[] args = result.Split("$$");
                        try
                        {
                            return handlerDB.AddAd(args[0], args[1], int.Parse(args[2]), int.Parse(args[3]), float.Parse(args[4], CultureInfo.InvariantCulture));
                        } catch (Exception e)
                        {
                            return "Problem occured:" + Environment.NewLine + e.Message;
                        }                        
                    }
                    return "Category wasn't added";
                default:
                    return "Wrong command, use \"help\" for additional information";
            }
        }

        private string GetHelp()
        {
            return @"You can use next commands:
""q"" - close programm
""help"" - open documentation
""all"" - get all elements
""users"" - get all users
""categories"" - get all categories
""ads"" - get all ads
""init tables"" - init tables (or throw error if tables are exist)
""init draft data"" - add 5 rows to each column
""add user"" - add user
""add category"" - add category
""add ad"" - add advertisement";
        }
    }
}
